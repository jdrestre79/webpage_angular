# Desarrollo de páginas con Angular - Coursera #

## Acerca de este Curso
En la actualidad las páginas web se han transformado en aplicaciones en sí mismas, con más cantidad de componentes que nunca antes, y con más exigencia de parte de sus consumidores y clientes.
En este curso aprenderás a utilizar Angular, uno de los frameworks líderes del mercado para desarrollo de aplicaciones de una única página, o conocidas como SPA por la sigla en inglés de 'Single Page Application'.
Además, se hará una introducción gradual al lenguaje NodeJS y al desarrollo de interfaces para aplicaciones (API por su sigla en inglés de Application Program Interface), con el objetivo de desarrollar servicios web básicos para darle comportamiento a la aplicación Angular.

Al finalizar este curso, habrás practicado con todas esas herramientas y serás capaz de diseñar una página web e implementar ese diseño con Bootstrap, crear un diseño responsive y preparar tu sitio web para salir a producción.

## Semana 1 - Introducción a Angular y primera SPA

En el módulo 1 aprenderás a crear un proyecto Angular y usar Typescript, aprenderás sus principales características y lo importante para tener en cuenta en la definición de tu proyecto. Iremos profundizando temas para que domines a fondo los conceptos técnicos y luego utilizaremos librerías y herramientas, como Express, que simplifican el proceso de desarrollo, por ejemplo, para simular una aplicación de servidor simple. Finalizado el primer módulo tendrás un conocimiento general de Typescript y buen criterio para estructurar en componentes una aplicación web de lado cliente SPA.

### Lista de lecciones
---
Lección | Descripción
:---: | :---
1 | Instalación de herramientas y primera aplicación angular
2 | Templates e Integración con Bootstap
3 | Manejo de Componentes en Angular
4 | Introducción a Typescript

## Semana 2 - 



### Lista de lecciones
---
Lección | Descripción
:---: | :---
1 | 

## Semana 3



### Lista de lecciones
---
Lección | Descripción
:---: | :---
1 | 

## Semana 4



### Lista de lecciones
---
Lección | Descripción
:---: | :---
1 | 

## Autor
Juan David Restrepo Z - [Github](https://github.com/jdrestre) | [Twitter](https://twitter.com/jdrestre) | [Linkedin](https://www.linkedin.com/in/juandavidrestrepozambrano/) | [Bitbucket](https://bitbucket.org/jdrestre79/)
